(() => {

  const toggler = document.querySelector('.navbar__menu_toggle_icon');

  toggler.addEventListener('click', function () {

    toggleNavlinkPadding()

    const navbarItems = document.querySelector('.navbar__items')

    let isVisible = navbarItems.classList.contains('visible');
    if (isVisible) {
      navbarItems.classList.remove('visible');
      toggler.style.opacity = 1
      return
    }
    navbarItems.classList.add('visible');
    toggler.style.opacity = 0.6
  })

  const toggleNavlinkPadding = () => {
    const navlinks = document.querySelectorAll('.navbar__navlink')

    navlinks.forEach(navlink => {
      navlink.classList.toggle('navlink__padding')
    })

  }
})()