class Pagination {
  constructor(postsPerPage) {
    this.posts = document.querySelectorAll(".post");
    this.postsAmount = this.posts.length;
    this.postsPerPage = postsPerPage;
    this.pagesAmount = Math.ceil(this.postsAmount / this.postsPerPage);
    this.paginationNode = document.querySelector(".pagination");
    this.currentPage = 1;
    this.rest = this.postsAmount % this.postsPerPage;
  }

  init() {
    this.displayNeccessaryPosts();
    this.handlePagination();
    this.handlePaginationButtons();
    // this.handleNavbarVisibility();
  }

  removeWindowListener() {
    window.onscroll = () => {}
  }

  addWindowListener() {
    const navbar = document.querySelector('.nav')
    const navbarHeight = navbar.clientHeight
    let prevScrollpos = window.pageYOffset;


    // здесь очень важен порядок выполнения условий

    window.onscroll = () => {
      const currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        navbar.style.position = 'fixed'
        navbar.style.top = "0";
      } else if (currentScrollPos < navbarHeight) {
        navbar.style.position = 'absolute'
      } else {
        navbar.style.top = `-${navbarHeight}px`;
      }
      prevScrollpos = currentScrollPos;
    }
  }

  displayNeccessaryPosts() {
    let postEnding = this.currentPage * this.postsPerPage
    let postStart = postEnding - this.postsPerPage
    if (this.currentPage === 1) {
      postEnding = this.postsPerPage;
      postStart = 0;
    }

    const isLastPage = this.currentPage === this.pagesAmount;
    this.hidePosts();

    if (this.postsPerPage > this.postsAmount) {
      this.posts.forEach(post => {
        post.classList.remove('hidePost')
        this.paginationNode.classList.add('hidePost')
      })
      return
    }
    if (isLastPage) {
      this.posts.forEach((post, i) => {
        if (i >= postStart) {
          post.classList.remove('hidePost')
        }
      })
      return
    }

    this.posts.forEach((post, i) => {
      if (i >= postStart && i < postEnding) {
        post.classList.remove('hidePost')
      }
    });
  }

  handlePagination() {
    this.paginationNode.addEventListener("click", event => {
      this.handlePrevClick(event);
      this.handleNextClick(event);
      this.handlePaginationButtons()
      this.removeWindowListener();
      this.displayNeccessaryPosts();
      this.addWindowListener();
    });
  }

  handlePrevClick(event) {
    const target = event.target.classList,
      parent = event.target.parentNode.classList
    if (target.contains("prev") || parent.contains("prev")) {
        this.currentPage += 1;
    }
  }

  handleNextClick(event) {
    const target = event.target.classList,
      parent = event.target.parentNode.classList
    if (target.contains("next") || parent.contains("next")) {
        this.currentPage -= 1;
    }
  }

  hidePosts() {
    this.posts.forEach((post) => {
      post.classList.add('hidePost')
    });
  }

  handlePaginationButtons() {
    const prevButton = document.querySelector('.prev')
    const nextButton = document.querySelector('.next')
    if (this.currentPage === 1) {
      prevButton.style.visibility = 'visible'
      nextButton.style.visibility = 'hidden'
      return
    }
    if (this.currentPage === this.pagesAmount) {
      nextButton.style.visibility = 'visible'
      prevButton.style.visibility = 'hidden'
      return
    }
    prevButton.style.visibility = 'visible'
    nextButton.style.visibility = 'visible'
  }
}

// pass a number to define how many posts
// will appear on your screen

const pagination = new Pagination(2);

pagination.init();