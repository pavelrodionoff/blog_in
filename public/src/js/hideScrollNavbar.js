//  https://www.w3schools.com/howto/howto_js_navbar_hide_scroll.asp

(() => {
const navbar = document.querySelector('.nav')
const navbarHeight = navbar.clientHeight
let prevScrollpos = window.pageYOffset;


// здесь очень важен порядок выполнения условий

window.onscroll = () => {
  navbar.style.position = 'absolute'
  const currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    navbar.style.position = 'fixed'
    navbar.style.top = "0";
  } else if (currentScrollPos < navbarHeight) {
    navbar.style.position = 'absolute'
  } else {
    navbar.style.top = `-${navbarHeight}px`;
  }
  prevScrollpos = currentScrollPos;
}
})() 